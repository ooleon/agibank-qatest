package localhost.entity;

import java.util.List;

public class ItinerarioUt {
	private String idlinha;
	private String nome;
	private String codigo;
//	private List<Parada> parada;
	
	
	
	public String getIdlinha() {
		return idlinha;
	}
	public void setIdlinha(String idlinha) {
		this.idlinha = idlinha;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	
}
