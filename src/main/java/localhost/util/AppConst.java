package localhost.util;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;

public class AppConst {
	
	public static String LINHAS_URL = "http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=l";
	public static String LINHAS_RECUSO = "/json/linhas.json";
	static InputStream inputStream = null;
	public static String LINHA_ATUALIZADA = "Linha Atualizada";
	public static String LINHA_APAGADA = "Linha Apagada";
	public static String BD_IMPORTADA = "Banco de dados Importada";
	public static String BD_NAOIMPORTADA = "Erro ao Banco de dados nao Importada corretamente";
	
	
	public static InputStream jsonParseDeUrl(String url, String jsonResourceLocal) {
		//bien
		inputStream=null;
		String t = conseguirUrl(url);
		if (t.isEmpty()) {
			System.out.println("Erro em URL, se usara recurso local: \n" + jsonResourceLocal);
			inputStream = TypeReference.class.getResourceAsStream(jsonResourceLocal);
		}else {
			inputStream = new ByteArrayInputStream(t.getBytes(StandardCharsets.UTF_8));
			System.out.println("json desde o URL: \n" + t);
		}
		//inputStream = TypeReference.class.getResourceAsStream(jsonString);
		//inputStream = new ByteArrayInputStream(jsonString.getBytes(StandardCharsets.UTF_8));

		return inputStream;
	}
	public static String conseguirUrl(String sUrl) {
		// bien
		String resp = "";
		// String
		// sUrl="http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=l";
		// url="https://jsonplaceholder.typicode.com/todos/1";
		try {
			// System.out.println(sUrl);
			RestTemplate restTemplate = new RestTemplate();
			resp = restTemplate.getForObject(sUrl, String.class);
			// List<String> resp = restTemplate.getForObject(sUrl, List.class);
			// System.out.println(resp );

		} catch (RestClientException e2) {
			e2.printStackTrace();
		}

		return resp;
	}
	
}
