package localhost.restcontroller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import localhost.entity.Linhas;
import localhost.injection.LinhasService;
import localhost.util.AppConst;
import localhost.util.NegocioException;

@RestController
@RequestMapping({ "/omnibustest" })
public class AdminRestController {
	public static final String ROOT_CONTROLLER = "/omnibustest";

	public static final String ADMINS = "/admins";
	public static final String STATE = "/state";
	public static final String ECHO = "/echo";
	public static final String BODY = "/body";
	public static final String STRING_LIST = "/string-list";
	public static final String DTO_LIST = "/dto-list";

	public static final String TUDOS = "/tudos";
	public static final String LINHAS = "/linhas";

	public static final String ATUALIZAR = "/atualizar";
	public static final String GUARDAR = "/guardar";
	public static final String PARAM_ID = "/{id}";
	public static final String PARAM = "/{parametro}";

	private static final Logger logger = LogManager.getLogger(AdminRestController.class);

	private List<Linhas> listLinhas;
	private InputStream inputStream = null;
	private String urlLinhas = "http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=l";
	private String recursoJsonFile = "/json/linhas.json";

	@Autowired
	private LinhasService linhasService;

	private String msgParser;

	@RequestMapping(value = { TUDOS }, method = { RequestMethod.GET })
	public ResponseEntity<List<Linhas>> getAllBlog() {
		return new ResponseEntity(this.linhasService.getAllLinhas(), HttpStatus.OK);
	}

	@RequestMapping(value = { LINHAS + PARAM_ID }, method = { RequestMethod.GET })
	public ResponseEntity<Linhas> getLinhasById(@PathVariable("id") String id) {
		logger.info(LINHAS + "/{id}" + " getLinhasById");
		return new ResponseEntity(this.linhasService.getLinhasById(id), HttpStatus.OK);
	}

	@RequestMapping(value = { LINHAS + "/nome" + "/{nome}" }, method = { RequestMethod.GET })
	public ResponseEntity<List<Linhas>> getLinhasByNome(@PathVariable("nome") String nome) {
		logger.info(LINHAS + "/nome " + nome);
		return new ResponseEntity(this.linhasService.getLinhasByNome(nome), HttpStatus.OK);
	}

	@RequestMapping(value = { GUARDAR }, method = { RequestMethod.POST })
	public ResponseEntity<Linhas> guardar(@RequestBody Linhas l) {
		return new ResponseEntity(this.linhasService.save(l), HttpStatus.OK);
	}

	@RequestMapping(value = { ATUALIZAR }, method = { RequestMethod.PUT })
	public ResponseEntity<String> atualizar(@RequestBody Linhas LinhasAnterior) {
		StringBuilder msg = new StringBuilder("");
		Linhas l = this.linhasService.getLinhasById(LinhasAnterior.getId().longValue());
		this.linhasService.save(l);
		msg.append(AppConst.LINHA_ATUALIZADA);
		logger.info(msg.toString());
		return new ResponseEntity(msg.toString(), HttpStatus.OK);
	}

	@RequestMapping(value = { LINHAS + PARAM_ID }, method = { RequestMethod.DELETE })
	public ResponseEntity<String> apagarPorId(@PathVariable("id") long id) {
		StringBuilder msg = new StringBuilder("");
		Linhas l = this.linhasService.getLinhasById(id);
		this.linhasService.remove(l);
		msg.append(AppConst.LINHA_APAGADA);
		logger.info(msg.toString());
		return new ResponseEntity(msg.toString(), HttpStatus.OK);
	}

	@RequestMapping(value = { "/importardados" }, method = { RequestMethod.GET })
	public ResponseEntity<String> importarLinhasByUrl() {
		StringBuilder msg = new StringBuilder("");
		int n = 0;
		try {
			n = this.getLinhasParse();
			if(!getMsgParser().isEmpty()) {
				msg.append(AppConst.BD_IMPORTADA + " Parcialmente com: " + n + " registos" );
			}else {
				msg.append(AppConst.BD_IMPORTADA + " Totalmente sem error com: " + n + " registos" );				
			}
		} catch (NegocioException e) {
			msg.append(AppConst.BD_NAOIMPORTADA);
			e.printStackTrace();
		} finally {
			
			logger.info(msg.toString());
			return new ResponseEntity(msg.toString(), HttpStatus.OK);
		}

	}

	public String conseguirUrl(String sUrl) {
		// bien
		String resp = "";
		// String
		// sUrl="http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=l";
		// url="https://jsonplaceholder.typicode.com/todos/1";
		try {
			// System.out.println(sUrl);
			RestTemplate restTemplate = new RestTemplate();
			resp = restTemplate.getForObject(sUrl, String.class);
			// List<String> resp = restTemplate.getForObject(sUrl, List.class);
			// System.out.println(resp );

		} catch (ResourceAccessException e3) {
			logger.info("Erro tentando acceder ao recuerso");
//			e3.printStackTrace();
		} catch (RestClientException e2) {
			e2.printStackTrace();
		} 

		return resp;

		/*
		 * JsonParser springParser = JsonParserFactory.getJsonParser(); Map<String,
		 * Object> map = springParser.parseMap(resp);
		 * 
		 * String mapArray[] = new String[map.size()];
		 * System.out.println("Items found: " + mapArray.length);
		 * 
		 * int i = 0; for (Map.Entry<String, Object> entry : map.entrySet()) {
		 * System.out.println(entry.getKey() + " = " + entry.getValue()); i++; }
		 */
	}

	public InputStream jsonParseDeUrl(String url, String jsonResourceLocal) {
		// bien
		String t = conseguirUrl(url);
		if (t.isEmpty()) {
			logger.info("Erro em URL, se usara recurso local: \n" + jsonResourceLocal);
			inputStream = TypeReference.class.getResourceAsStream(jsonResourceLocal);
		} else {
			inputStream = new ByteArrayInputStream(t.getBytes(StandardCharsets.UTF_8));
			logger.info("json desde o URL: \n" + t);
		}

		return inputStream;
	}

	public int getLinhasParse() throws NegocioException{
		int n=0;
		try {
			InputStream is = jsonParseDeUrl(urlLinhas, recursoJsonFile);
			// read JSON and load json
			ObjectMapper mapper = new ObjectMapper();
			TypeReference<List<Linhas>> typeReference = new TypeReference<List<Linhas>>() {
			};
			listLinhas = mapper.readValue(is, typeReference);
			if(listLinhas != null && !listLinhas.isEmpty() ) {
				logger.info("LL Inicio: " + listLinhas.size() + "\n" + listLinhas.get(0));
				msgParser="";
			for (Linhas linhas : listLinhas) {
				try {
					linhasService.save(linhas);
				} catch (DataIntegrityViolationException e) {
					msgParser="DataIntegrityViolationException";
					logger.info("DataIntegrityViolationException");
//					e.printStackTrace();
				}
			}
			n=linhasService.getAllLinhas().size();
			logger.info("LL Final: " + (n));

			}else {
				logger.info("Erro na importação dados");
				throw new NegocioException(1003, "Erro na importação dados");
			}

		} catch (IOException e) {
			logger.info(e.getMessage());
			logger.info(e.getStackTrace().toString());
		}
		return n;
	}

	@RequestMapping(value = { "/vaziarbd" }, method = { RequestMethod.GET })
	public ResponseEntity<Linhas> apagarTodaBd() {
		StringBuilder msg = new StringBuilder("");
		msg.append(this.linhasService.apagarBD());
		logger.info(msg.toString());
		return new ResponseEntity(msg.toString(), HttpStatus.OK);
	}

	public String getMsgParser() {
		return msgParser;
	}

	public void setMsgParser(String msgParser) {
		this.msgParser = msgParser;
	}

}
