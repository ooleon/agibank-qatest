package localhost.restcontroller;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import localhost.util.NegocioException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import localhost.entity.Linhas;
import localhost.injection.LinhasService;
import localhost.util.AppConst;
import localhost.util.NegocioException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LinhasRestControllesMockTest {

	@MockBean
	LinhasService linhasServiceMock;
	@MockBean
	Linhas linhasMock;
	@MockBean
	List<Linhas> listLinhasMock; 

	@Autowired
	AdminRestController adminRestController;

	Linhas linhas;

	@Before
	public void setupMock() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testMockCreation() {
		assertNotNull(linhasMock);
		assertNotNull(linhasServiceMock);
	}

	@Test
	public void shouldReturnLinha_whenGuardarEChamado() throws Exception {
		when(linhasServiceMock.save(linhasMock)).thenReturn(linhasMock);

		Linhas savedLinhas = adminRestController.guardar(linhasMock).getBody();

		assertEquals(savedLinhas, linhasMock);
	}

	@Test
	public void testGetLinhasById() {
		when(linhasMock.getId()).thenReturn(90L);
		when(linhasServiceMock.getLinhasById(linhasMock.getId().toString() )).thenReturn(linhasMock);
		
		Linhas getLinhasById = adminRestController.getLinhasById(linhasMock.getId().toString()).getBody();

		assertEquals(getLinhasById, linhasMock);
	}

	@Test
	public void testGetLinhasByNome() {
		when(linhasMock.getId()).thenReturn(90L);

		when(linhasServiceMock.getLinhasByNome(linhasMock.getNome() )).thenReturn( listLinhasMock);
		
		List<Linhas> listLinhasMock  = adminRestController.getLinhasByNome(linhasMock.getNome()).getBody();

		assertEquals(listLinhasMock, listLinhasMock);
	}
	
	
	@Test
	public void shouldReturnString_whenApagarEChamado(){

		String apagadaLinha = adminRestController.apagarPorId(linhasMock.getId()).getBody();

		assertEquals(apagadaLinha, AppConst.LINHA_APAGADA);
	}
	 

}
