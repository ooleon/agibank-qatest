package localhost;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import localhost.util.NegocioException;
import localhost.util.NegocioValidation;

//@RunWith(SpringRunner.class)
//@SpringBootTest
@RunWith(BlockJUnit4ClassRunner.class)
public class Exception2Test {

	@Before
	public void setUpTest() {
	}

	@Test(expected = NegocioException.class)
	public void testNegocioException() throws NegocioException {
		if (NegocioValidation.validarAlgoVardadero() == Boolean.TRUE) {
			
	        throw new NegocioException(1002, "Erro de Negocio");
	        
		} else {
			fail();
		}
	}

    
}
