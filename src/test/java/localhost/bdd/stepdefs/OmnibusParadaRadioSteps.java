package localhost.bdd.stepdefs;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import io.cucumber.core.api.Scenario;
import io.cucumber.java.en.*;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import localhost.OmnibusParadaRadioTests;
import localhost.util.LinhasParser;

public class OmnibusParadaRadioSteps {
	private String radio;
	private String resultado;
	private String latitudeCentro;
	private String longitudeCentro;
	private String latitudeBuqueda;
	private String longitudeBuqueda;
	private String resultadoEsperado; 
	private String resultadoActual;
	OmnibusParadaRadioTests o = new OmnibusParadaRadioTests();


	@Given("^o ponto de referencia central latitude \"([^\"]*)\" e longitude \"([^\"]*)\", o radio \"([^\"]*)\"$")
	public void cargarCentroeRadio(String latitudeCentro, String longitudeCentro, String radio) {
//		System.out.println("radio: " + radio);
		this.radio = radio;
		this.latitudeCentro = latitudeCentro;
		this.longitudeCentro = longitudeCentro;
	}

	@And("^a parada a calcular com latitude \"([^\"]*)\" e longitude \"([^\"]*)\" e o radio$")
	public void cargarLatLng(String latitudeBuqueda, String longitudeBuqueda) {
//		System.out.println("lat: " + latitudeBuqueda + " lng: " + longitudeBuqueda);
		this.latitudeBuqueda = latitudeBuqueda;
		this.longitudeBuqueda = longitudeBuqueda;
		
	}

	@When("^usuario questiona onde esta a parada")
	public void quandoQuestionaParada() {
//		System.out.println("quandoQuestionaParada");

	}

	@Then("^parada \"([^\"]*)\"$")
	public void entaoParada(String resultadoEsperado) {
//		System.out.println("resultadoEsperado " + resultadoEsperado);
		resultadoActual = LinhasParser.distanciaCirculo(
				this.latitudeCentro, this.longitudeCentro, 
				Double.valueOf(this.radio),
				this.latitudeBuqueda, this.longitudeBuqueda);
//		System.out.println("resultadoActual " + resultadoActual);
        assertEquals(resultadoEsperado, resultadoActual);

	}

	@Test
	public void distanciaDentroCirculoTest() {
		double radioD = 18.579778416017447;

		resultado = LinhasParser.distanciaCirculo("-30.05213479817800000", "-51.22836428234100000", radioD,
				"-30.00868779817800000", "-51.14298528234100000");
//		System.out.println(resultado);
		assertEquals("está dentro do círculo", resultado);
	}

	@Test
	public void distanciaCirculoForaTest() {
		String resultado;

		resultado = LinhasParser.distanciaCirculo("-30.05213479817800000", "-51.22836428234100000", 8.579778416017447,
				"-30.00868779817800000", "-51.14298528234100000");
//		System.out.println(resultado);
		assertEquals("está fora do círculo", resultado);
	}

	@Test
	public void distanciaNoCirculoTest() {
		String resultado;

		resultado = LinhasParser.distanciaCirculo("-30.05213479817800000", "-51.22836428234100000", 9.579778416017447,
				"-30.00868779817800000", "-51.14298528234100000");
//		System.out.println(resultado);
		assertEquals("está no círculo", resultado);
	}

}
