package localhost.bdd.stepdefs;

import io.cucumber.core.api.Scenario;
//import io.cucumber.java.pt.*;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Mas;
import io.cucumber.java.pt.Quando;
import io.cucumber.java.en.*;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
//import org.junit.runner.RunWith;

//@RunWith(SpringRunner.class)
//@SpringBootTest
//@RunWith(Cucumber.class)
//@CucumberOptions(plugin = "pretty", features = "src/test/resources/localhost/omnibustestApplicationTests.feature")
public class OmnibustestApplicationSteps {
    private String today;
    private String actualAnswer;

//    @Given("^today is \"([^\"]*)\"$")
    @Dado("^today is \"([^\"]*)\"$")
    public void today_is(String today) {
        this.today = today;
    }
    
//    @Given("^today is Sunday$")
//    public void today_is_Sunday() {
//        today = "Sunday";
//    }

//  @When("^I ask whether it's Friday yet$")
    @Quando("^I ask whether it's Friday yet$")
    public void i_ask_whether_it_s_Friday_yet() {
        this.actualAnswer = IsItFriday.isItFriday(today);
    }
//    <version>${spring-restdocs.version}</version>
//    @Then("^I should be told \"([^\"]*)\"$")
    @Entao("^I should be told \"([^\"]*)\"$")
    public void i_should_be_told(String expectedAnswer) {
//    	System.out.println(expectedAnswer);
//    	System.out.println(actualAnswer);
        assertEquals(expectedAnswer, actualAnswer);
    }


    @Dado("^algo mais")
    @Before
	public void setupBefore() {
	}
	


}
class IsItFriday {
    static String isItFriday(String today) {
	return "Friday".equals(today) ? "TGIF" : "Nope";
    }
}
