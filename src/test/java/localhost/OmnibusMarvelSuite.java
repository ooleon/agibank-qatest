package localhost;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.springframework.boot.test.context.TestComponent;

//@RunWith(SpringRunner.class)
//@SpringBootTest
@RunWith(Suite.class)
@SuiteClasses({Exception1Test.class, Exception2Test.class, OmnibusMarvelTest.class})
public class OmnibusMarvelSuite {
}
