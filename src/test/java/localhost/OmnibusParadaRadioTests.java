package localhost;

import org.junit.runner.RunWith;

import io.cucumber.junit.CucumberOptions;
import localhost.bdd.CucumberReportRunner;

@RunWith(CucumberReportRunner.class)
@CucumberOptions(plugin = {"pretty","json:target/cucumberCirculo-report.json"},
features = "src/test/resources/localhost/OmnibusParadaRadioTest.feature")
public class OmnibusParadaRadioTests {

}
