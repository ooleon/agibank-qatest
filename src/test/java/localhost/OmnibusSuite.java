package localhost;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import localhost.util.NegocioException;
import static junit.framework.TestCase.fail;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.springframework.boot.test.context.TestComponent;


//@RunWith(SpringRunner.class)
//@SpringBootTest
@RunWith(Suite.class)
@SuiteClasses({Exception1Test.class, Exception2Test.class})
public class OmnibusSuite {

	@Test(expected = NegocioException.class)
	public void testNegocioException() throws NegocioException {
        throw new NegocioException(1002, "Erro de Negocio");
	}

}
