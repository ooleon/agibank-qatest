# Projeto
[![pipeline status](https://gitlab.com/ooleon/agibank-qatest/badges/master/pipeline.svg)](https://gitlab.com/ooleon/agibank-qatest/commits/master)
[![coverage report](https://gitlab.com/ooleon/agibank-qatest/badges/master/coverage.svg)](https://gitlab.com/ooleon/agibank-qatest/commits/master)

Projeto de exemplo com Spring Boot Java JDK8 para mostrar uso de TDD, BDD, Test unitarios, Test de exception, com algumas tecnologias como Servicios Rest, JPA, h2, elastic search,

No seguinte  projeto que o cliente pedeu privado no GitLab,  está disponibilizada esta documentação com descrição, configuração e execução),  esta desenvolvida em Java com Spring Boot com seu respectivo gerenciador de projeto ( Maven);
Foi escolhida a API pública ( https://  /) e com tem uma API para a importação dos dados, com tem uma API CRUD para o banco de datos, teste positivos para cada método do CRUD, Adicionar, Pesquisar, Modificar, Apagar e listar individualmente e listar todos; tambem com tem adicionalmente, dois testes para excessões;

## Tecnologias

As tecnologias usadas são:

- [JUnit4]
- [Maven]
- [SpringBoot] 2.1.6
- [Cucumber] 4.7.1
- [Java] 1.8

### Instalacao / Run

* Git Clone from GitLab
* mvn -q test

>Cada vez que tiver uma mudança no codigo gitlab rodara o Testes.

### Relatorio
cucumber-report.json

# Autor Leon Osty [Linkedin]

[Maven]: <https://maven.apache.org/>
[JUnit4]: <https://junit.org/junit4/>
[SpringBoot]: <https://spring.io/blog/2019/06/19/spring-boot-2-1-6-released>
[Linkedin]: <https://www.linkedin.com/in/ostyleon/>
[Java]: <https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html>
[Cucumber]: <https://cucumber.io/>
